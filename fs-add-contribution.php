<div class="fs-main-wrap">
	<h1 class="fs-page-title"><i class="icon-dollar"></i> Funding Serious - Add a Contribution</h1>
	<div id="fs-settings-wrap">
		<p>
			<label><?php _e('Select a Campaign','fs'); ?></label>
			<?php $campaigns = $this->fs_get_all_campaigns(); ?>
			<?php if($campaigns) :  ?>
			<form method="get" action="">						
				<input type="hidden" name="page" value="fs-contribution" />
				<select name="campaign" class="fs-campaign">
					<option></option>
					<?php foreach($campaigns as $campaign) : ?>
						<option value="<?php echo $campaign->ID; ?>" <?php if(isset($_GET['campaign']) && $_GET['campaign'] == $campaign->ID){echo 'selected="selected"';}?>><?php echo $campaign->post_title; ?></option>
					<?php endforeach; ?>
				</select>
				<input type="submit" value="Load Form" />
			</form>
			<?php else : ?>
				<?php printf(__('No Campaigns were found. %1$sCreate one here%2$s','fs'),'<a href="/wp-admin/post-new.php?post_type=fs_campaign">','</a>'); ?>
			<?php endif; ?>
		</p>
		<hr/>
		<?php if(isset($_GET['campaign'])) : ?>
			<div class="fs-campaign-gravity-form">
            	<?php 
            		$campaign_id = $_GET['campaign'];

            		// check for micro campaign
            		if($parent_id = get_post_meta($campaign_id, 'fs_parent_campaign', true)){
						$form_id = get_post_meta($parent_id,'fs_gravity_form',true);
            		}else{
						$form_id = get_post_meta($campaign_id,'fs_gravity_form',true);
            		}
            	
            		// do shortcode
           			echo do_shortcode('[gravityform id="'.$form_id.'" title="false" description="false" ajax="true"]'); 
           		?>
			</div>
		<?php endif; ?>
	</div>
</div>